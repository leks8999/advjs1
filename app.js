// Ответы на теоретические вопросы:
// 1. Смысл прототипного наследования в том, что один объект можно сделать прототипом другого.
// При этом если свойство не найдено в объекте — оно берётся из прототипа.
// 2. super() вызывает конструктор наследуемого класса.
// Это необходимо когда вам нужно получить доступ к переменным наследуемого класса.

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  get sal() {
    return this.salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get sal() {
    return this.salary * 3;
  }
}

const programmer1 = new Programmer("leks", 30, 100, "eng");
const programmer2 = new Programmer("leks", 50, 200, "deu");

console.log(programmer1);
console.log(programmer2);
console.log(programmer1.sal);
console.log(programmer2.sal);
